import React, { Component } from 'react';
import './App.css';

/* Components */
/* 
import Image from './components/Image';
import Input from './components/Input';
*/

//METODOS DE CICLO DE VIDA
/*
  componentWillMount: 
    para llamadas a API o DB
    previo a renderezar la pantalla

     componentDidMount: 
    Para hacer llamadas luego de renderezar

  componentWillUnMount: 
    utilizado cuando un componente se va a desmontar

  componentWillReceiveProps
  
  componentShouldUpdate

  componentWillUpdate
*/

import Children from './components/Children'
 

class App extends Component {

    constructor(){
      super();

      this.state = {
        inputChildValue: "10"
      }

      this.comunicaHijo = this.comunicaHijo.bind(this);
    }
    comunicaHijo(e){
      //this.refs.children.escribeHola();
      this.setState({inputChildValue: "20"});
    }
    muestraAlerta(number){
      alert(number);
    }

    render() {
    return (
      <div>
        <button onClick={this.comunicaHijo}>Comunica al hijo</button>
        <Children 
                ref="children" 
                inputValue={this.state.inputChildValue} 
                muestraAlerta={this.muestraAlerta}/>
      </div>
    );
  }
}

export default App;
