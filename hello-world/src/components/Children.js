import React, {Component} from 'react';
import PropTypes from 'prop-types'
class Children extends Component{
    constructor(props){
        super(props);
        this.state = {
            inputValue: this.props.inputValue
        };
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.inputValue !== this.state.inputValue) {
            this.setState({inputValue: nextProps.inputValue});
        }
    }

    escribeHola(){
        this.setState({inputValue:"Hola accionado desde el padre"})
    }
    render(){
        return(
            <div>
                <input 
                    value={this.state.inputValue}
                    placeholder = "Entrada hijo"
                    onChange= {(e)=>{this.setState({inputValue: e.target.value})}}
                    />
                <button onClick={(e) => {this.props.muestraAlerta(100)}}>Boton hijo</button>
            </div>
        );
    }
}

Children.propTypes = {
    inputValue: PropTypes.string,
    muestraAlerta: PropTypes.finc
}

export default Children;